import React from "react";

const GenreList = (props) => {
  const { items, onItemSelect, selectedItem } = props;
  return (
    <ul className="list-group">
      {items.map((genre) => (
        <li
          className={
            selectedItem === genre
              ? "list-group-item active"
              : "list-group-item"
          }
          key={genre._id}
          onClick={() => onItemSelect(genre)}
        >
          {genre.name}
        </li>
      ))}
    </ul>
  );
};

export default GenreList;
